# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the kmail package.
#
# SPDX-FileCopyrightText: 2024 Toms Trasuns <toms.trasuns@posteo.net>
msgid ""
msgstr ""
"Project-Id-Version: kmail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-02 01:32+0000\n"
"PO-Revision-Date: 2024-02-01 10:31+0200\n"
"Last-Translator: Toms Trasuns <toms.trasuns@posteo.net>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Toms Trasūns"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "toms.trasuns@posteo.net"

#: main.cpp:28 main.cpp:30
#, kde-format
msgid "KMail Assistant for refreshing settings"
msgstr "„KMail“ iestatījumu pārlādēšanas asistents"

#: main.cpp:32
#, kde-format
msgid "(c) 2019-2024 Laurent Montel <montel@kde.org>"
msgstr "(c) 2019–2024 Laurent Montel <montel@kde.org>"

#: main.cpp:33
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: main.cpp:33
#, kde-format
msgid "Author"
msgstr "Autors"

#: refreshsettingsassistant.cpp:22
#, kde-format
msgctxt "@title:window"
msgid "KMail Refresh Settings"
msgstr "„KMail“ iestatījumu pārlādētājs"

#: refreshsettingsassistant.cpp:39
#, kde-format
msgid "Warning"
msgstr "Brīdinājums"

#: refreshsettingsassistant.cpp:43
#, kde-format
msgid "Clean up Settings"
msgstr "Uzkopt iestatījumus"

#: refreshsettingsassistant.cpp:47
#, kde-format
msgid "Finish"
msgstr "Pabeigt"

#: refreshsettingscleanuppage.cpp:20
#, kde-format
msgid "Clean"
msgstr "Uzkopt"

#: refreshsettingscleanuppage.cpp:50
#, kde-format
msgid "Remove obsolete \"TipOfDay\" settings: Done"
msgstr "Noņemt novecojušas „Dienas padoma“ iestatījumus: pabeigts."

#: refreshsettingscleanuppage.cpp:62
#, kde-format
msgid "Delete Dialog settings in file `%1`: Done"
msgstr "Dzēst loga iestatījumus datnē „%1“: pabeigts"

#: refreshsettingscleanuppage.cpp:74
#, kde-format
msgid "Delete Filters settings in file `%1`: Done"
msgstr "Dzēst filtru iestatījumus datnē „%1“: pabeigts"

#: refreshsettingscleanuppage.cpp:90
#, kde-format
msgid "Clean Folder Settings in setting file `%1`: Done"
msgstr "Uzkopt mapju iestatījumu datnē „%1“: pabeigts"

#: refreshsettingscleanuppage.cpp:146
#, kde-format
msgid "Clean Dialog Size in setting file `%1`: Done"
msgstr "Uzkopt loga izmēra iestatījumus datnē „%1“: pabeigts"

#: refreshsettingsfirstpage.cpp:18
#, kde-format
msgid "Please close KMail/Kontact before using it."
msgstr "Pirms izmantošanas aizveriet „KMail“/„Kontact“."
